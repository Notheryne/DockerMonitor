namespace DockerMonitor.Data;
using Docker.DotNet;
using Docker.DotNet.Models;

public class DockerService
{
    static string dockerUrl = "unix:///var/run/docker.sock";
    DockerClient client = new DockerClientConfiguration().CreateClient();

    public string SetDockerUrl(string url)
    {
        if (url != "")
        {
            try
            {
                var uri = new Uri(url);
                dockerUrl = url;
                client = new DockerClientConfiguration(uri).CreateClient();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return e.Message;
            }
        } else {
            dockerUrl = "";
            client = new DockerClientConfiguration().CreateClient();
        }
        return "";
    }

    public string GetDockerUrl()
    {
        return dockerUrl;
    }

    public DockerClient GetDockerClient()
    {
        return client;
    }

    public async Task<ContainerListResponse[]> GetContainersAsync()
    {
        var containers = await client.Containers.ListContainersAsync(
            new Docker.DotNet.Models.ContainersListParameters()
        );

        return containers.OrderBy(container => container.ID).ToArray();
    }

    public async Task<ImagesListResponse[]> GetImagesAsync()
    {
        var images = await client.Images.ListImagesAsync(
            new Docker.DotNet.Models.ImagesListParameters()
        );

        return images.OrderBy(image => image.Created).ToArray();
    }

    public async Task<VolumeResponse[]> GetVolumesAsync()
    {
        var volumes = await client.Volumes.ListAsync(
            new Docker.DotNet.Models.VolumesListParameters()
        );

        return volumes.Volumes.OrderBy(volume => volume.CreatedAt).ToArray();
    }

    public async Task<TaskResponse[]> GetTasksAsync()
    {
        var tasks = await client.Tasks.ListAsync(
            new Docker.DotNet.Models.TasksListParameters()
        );

        return tasks.ToArray();
    }

    public async Task<VersionResponse> GetVersionAsync()
    {
        var version = await client.System.GetVersionAsync();
        return version;
    }
    public async Task<SystemInfoResponse> GetSystemInfoAsync()
    {
        var systemInfo = await client.System.GetSystemInfoAsync();
        return systemInfo;
    }

    // public async Task<string[]> GetContainerEvensAsync()
    // {
    //     var containerEvents = await client.System.MonitorEventsAsync(
    //         new ContainerEventsParameters()
    //     );
    // }
}
